import Vue from 'vue'
import VueResource from 'vue-resource'
import App from 'pages/App.vue'
import VueRouter from 'vue-router'
import router from './router/index'

Vue.use(VueRouter)
Vue.use(VueResource)
Vue.config.silent = true;

router.beforeEach( (to, from, next) => {

    if (to.matched.some(record => record.meta.requiresAuth)) {
        // этот путь требует авторизации, проверяем залогинен ли
        // пользователь, и если нет, перенаправляем на страницу логина


        fetch('/admin').then((response) => {
            let is_login = response.status;
            console.log("login: ", is_login)
            if (response.url.includes("login")) {
                next({
                    name: 'login',
                    params: {to:"editNews"}
                })
            } else {
                next()
            }
        })
    } else {
        next() // всегда так или иначе нужно вызвать next()!
    }
})

let vue = new Vue({
    render: h => h(App),
    el: '#app',
    router
})

