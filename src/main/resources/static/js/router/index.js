import VueRouter from "vue-router";
import Main from "../pages/Main.vue";
import About from '../components/index/about/about.vue'
import Products from '../components/index/products.vue'
import News from '../components/index/news/news.vue'
import Article from '../components/index/article/article.vue'
import Career from '../components/index/career.vue'

import Admin from "../pages/Admin.vue";

import ArticlesEditor from "../components/admin/articlesEditor.vue"
import ArticleEditor from "../components/admin/article/articleEditor.vue"

import UsersEditor from "../components/admin/usersEditor.vue"
import UserEditor from "../components/admin/userEditor.vue"

import ProductsEditor from "../components/admin/productsEditor.vue"
import ProductEditor from  "../components/admin/productEditor.vue"

import Login from "../pages/Login.vue"

export default new VueRouter({
    routes:[
        {
            name: "index",
            path: "/",
            component: Main,
            children:[
                {
                    name: "about",
                    path: "/",
                    component: About
                },
                {
                    name: "products",
                    path: "products",
                    component: Products
                },
                {
                    name: "news",
                    path: "news",
                    component: News
                },
                {
                    name: "articles",
                    path: "articles/:id",
                    component: Article
                },
                {
                    name: "career",
                    path: "career",
                    component: Career
                }
            ]
        },
        {
            name: "login",
            path: "/login/:to",
            component: Login
        },
        {
            name: "admin",
            path: "/admin",
            component: Admin,
            meta:{requiresAuth: true},
            children:[
                {
                    name: "editNews",
                    path: "/",
                    component: ArticlesEditor,
                    meta:{
                        id:0
                    }
                },
                {
                    name: "editProducts",
                    path: "editProducts",
                    component: ProductsEditor,
                    meta:{
                        id:1
                    }
                },
                {
                    name: "editUsers",
                    path: "editUsers",
                    component: UsersEditor,
                    meta:{
                        id:2
                    }
                },
                {
                    name: "editUser",
                    path: "editUser/:isNew/:id",
                    component: UserEditor
                },
                {
                    name: "editArticle",
                    path: "editArticle/:isNew/:id",
                    component: ArticleEditor
                },
                {
                    name: "editProduct",
                    path: "editProduct/:isNew/:id",
                    component: ProductEditor
                },
            ]
        }
    ]
})