package hms.website.services;

import hms.website.domain.Article;

import java.util.List;

public interface ArticleService {
    Article save(Article article);
    List<Article> getAll();
    List<Article> get(Long from, Long count);
    Article getById(Long id);
    List<Article> getNewest();
    void delete(Article article);
}
