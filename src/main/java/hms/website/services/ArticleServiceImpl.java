package hms.website.services;

import hms.website.domain.Article;
import hms.website.domain.Chapter;
import hms.website.domain.ChapterBlock;
import hms.website.exeption.NotFoundException;
import hms.website.repo.ArticlesRepo;
import hms.website.repo.ChapterBlockRepo;
import hms.website.repo.ChapterRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ArticleServiceImpl implements ArticleService {
    private final ArticlesRepo articlesRepo;
    private final ChapterRepo chapterRepo;
    private final ChapterBlockRepo chapterBlockRepo;
    private final static Logger logger = LoggerFactory.getLogger(ArticleServiceImpl.class);

    public ArticleServiceImpl(ArticlesRepo articlesRepo, ChapterRepo chapterRepo, ChapterBlockRepo chapterBlockRepo) {
        this.articlesRepo = articlesRepo;
        this.chapterRepo = chapterRepo;
        this.chapterBlockRepo = chapterBlockRepo;
    }

    @Override
    public Article save(Article article) {
        Article article1 = articlesRepo.save(article);
        article.getChapters().forEach(chapter -> {
            logger.info("{}", chapter);
            chapterRepo.save(chapter);
            chapterBlockRepo.saveAll(chapter.getBlocks());
        });
        return article1;
    }

    @Override
    public List<Article> getAll() {
        return StreamSupport.stream(articlesRepo.findAll().spliterator(), false).sorted((a, b) -> {
            Article a1 = (Article)a;
            Article b1 = (Article)b;
            return b1.getDate().compareTo(a1.getDate());
        } ).collect(Collectors.toList());
    }

    @Override
    public List<Article> get(Long from, Long count) {
        return StreamSupport.stream(articlesRepo.findAll().spliterator(), false).sorted((a, b) -> {
            Article a1 = (Article)a;
            Article b1 = (Article)b;
            return b1.getDate().compareTo(a1.getDate());
        } ).skip(from).limit(count).collect(Collectors.toList());
    }

    @Override
    public Article getById(Long id) {
        return articlesRepo.findById(id).orElseThrow(NotFoundException::new);
    }

    @Override
    public List<Article> getNewest() {
        logger.info("{}=>{}", articlesRepo.findAll(), articlesRepo.findFirst6ByOrderByDateDesc());
        return articlesRepo.findFirst6ByOrderByDateDesc();
    }

    @Override
    public void delete(Article article) {
        article.getChapters().forEach(chapter -> {
            chapterBlockRepo.deleteAll(chapter.getBlocks());
            chapterRepo.delete(chapter);
        });
        articlesRepo.delete(article);
    }
}
