package hms.website.services;

public interface EmailService {
    void sendMessage(String to, String subject, String messageText, String pathToAttachment);
    void sendMessage(String to, String subject, String messageText);
}
