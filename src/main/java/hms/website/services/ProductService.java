package hms.website.services;

import hms.website.domain.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAll();
    Product save(Product product);
    void delete(Product product);

    Product getById(Long id);
}
