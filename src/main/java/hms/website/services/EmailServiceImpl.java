package hms.website.services;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Optional;

@Service
public class EmailServiceImpl implements EmailService{

    private final JavaMailSender mailSender;
    private final String from;
    private final static Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);

    public EmailServiceImpl(JavaMailSender mailSender, @Value("${spring.mail.from}") String from) {
        this.mailSender = mailSender;
        this.from = from;
        logger.info(from);
    }
    @Override
    @SneakyThrows
    public void sendMessage(String to, String subject, String messageText, String pathToAttachment) {
        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setFrom(from);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(messageText);

        File resume = new File(pathToAttachment);

        FileSystemResource file = new FileSystemResource(resume);
        helper.addAttachment("Resume."+ Optional.ofNullable(file.getFilename())
                .filter(f -> f.contains("."))
                .map(f -> f.substring(file.getFilename().lastIndexOf(".") + 1)).orElseGet(()->""), file);

        mailSender.send(message);
        resume.delete();
    }
    @Override
    @SneakyThrows
    public void sendMessage(String to, String subject, String messageText) {
        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setFrom(from);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(messageText);

        mailSender.send(message);
    }
}
