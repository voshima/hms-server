package hms.website.services;

import hms.website.domain.Product;
import hms.website.exeption.NotFoundException;
import hms.website.repo.ProductsRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ProductServiceImpl implements ProductService{
    private final ProductsRepo productsRepo;

    public ProductServiceImpl(ProductsRepo productsRepo) {
        this.productsRepo = productsRepo;
    }

    @Override
    public List<Product> getAll() {
        return StreamSupport.stream(productsRepo.findAll().spliterator(), false).collect(Collectors.toList());
    }

    @Override
    public Product save(Product product) {
        return productsRepo.save(product);
    }

    @Override
    public void delete(Product product) {
        productsRepo.delete(product);
    }

    @Override
    public Product getById(Long id) {
        return productsRepo.findById(id).orElseThrow(NotFoundException::new);
    }
}
