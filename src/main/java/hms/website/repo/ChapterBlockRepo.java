package hms.website.repo;

import hms.website.domain.ChapterBlock;
import org.springframework.data.repository.CrudRepository;

public interface ChapterBlockRepo extends CrudRepository<ChapterBlock, Long> {
}
