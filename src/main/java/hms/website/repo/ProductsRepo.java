package hms.website.repo;


import hms.website.domain.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductsRepo extends CrudRepository<Product, Long> {
}
