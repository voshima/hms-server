package hms.website.repo;

import hms.website.domain.Chapter;
import org.springframework.data.repository.CrudRepository;

public interface ChapterRepo extends CrudRepository<Chapter, Long> {
}
