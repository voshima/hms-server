package hms.website.repo;

import hms.website.domain.Article;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface ArticlesRepo extends CrudRepository<Article, Long> {
    List<Article> findFirst6ByOrderByDateDesc();
}
