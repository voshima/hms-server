package hms.website.controller;

import hms.website.domain.Article;
import hms.website.services.ArticleService;
import hms.website.services.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("articles")
public class ArticlesController {
    private final ArticleService articleService;
    private final StorageService storageService;

    @Autowired
    public ArticlesController(ArticleService articleService, StorageService storageService) { this.articleService = articleService;
        this.storageService = storageService;
    }

    @GetMapping
    public List<Article> list(@RequestParam(name = "from", defaultValue = "0", required = false) Long from,@RequestParam(name = "count", defaultValue = "6", required = false) Long count) {
        return articleService.get(from, count);
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @GetMapping("all")
    public List<Article> getAll()
    {
        return articleService.getAll();
    }

    @GetMapping("slider")
    public List<Article> getLast6Newest() {
        return articleService.getNewest();
    }

    @GetMapping("{id}")
    public Article getOne(@PathVariable("id") Long id) {
        return articleService.getById(id);
    }
}
