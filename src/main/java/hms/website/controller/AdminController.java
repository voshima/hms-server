package hms.website.controller;

import hms.website.domain.Article;
import hms.website.domain.Product;
import hms.website.domain.User;
import hms.website.services.ArticleService;
import hms.website.services.ProductService;
import hms.website.services.UserService;
import hms.website.services.storage.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("admin")
public class AdminController {
    private final Logger logger = LoggerFactory.getLogger(AdminController.class);
    private final ArticleService articleService;
    private final StorageService storageService;
    private final ProductService productService;
    private final UserService userService;

    public AdminController(ArticleService articleService, StorageService storageService, ProductService productService, UserService userService) {
        this.articleService = articleService;
        this.storageService = storageService;
        this.productService = productService;
        this.userService = userService;
    }

    @PostMapping("newArticle")
    public void newArticle(@RequestBody Article article, Authentication authentication){
        User author = (User)authentication.getPrincipal();
        article.setAuthor(author.getAuthorNick());
        articleService.save(article);
    }

    @PutMapping("editArticle/{id}")
    public void editArticle(@RequestBody Article article, @PathVariable("id") Long id){
        articleService.save(article);
    }

    @DeleteMapping("deleteArticle/{id}")
    public void deleteArticle(@PathVariable("id") Long id){
        articleService.delete(articleService.getById(id));
    }

    @PostMapping("newProduct")
    public void newProduct(@RequestBody Product product){
        logger.info("{}", product);
        productService.save(product);
    }

    @PutMapping("editProduct/{id}")
    public void editProduct(@RequestBody Product product, @PathVariable("id") Long id){
        productService.save(product);
    }

    @DeleteMapping("deleteProduct/{id}")
    public void deleteProduct(@PathVariable("id") Long id){
        productService.delete(productService.getById(id));
    }

    @PostMapping("uploadImg")
    public String uploadImg(@RequestParam(value = "file", required = false) MultipartFile file){
        String name = storageService.store(file);
        return "{\"url\": \"/articles/files/"+name+"\"}";
    }

    @PostMapping("newUser")
    public void newUser(@RequestBody User user){
        userService.saveUser(user);
    }

    @PutMapping("editUser/{id}")
    public void editUser(@RequestBody User user_new, @PathVariable("id") Long id){
        User user = userService.findUserById(id);
        user.setUsername(user_new.getUsername());
        user.setAuthorNick(user_new.getAuthorNick());
        userService.saveUser(user);
    }

    @DeleteMapping("deleteUser/{id}")
    public void deleteUser(@PathVariable("id") Long id){
        userService.deleteUser(id);
    }

    @GetMapping("users")
    public List<User> getUsers(){
        return userService.allUsers();
    }

    @GetMapping("users/{id}")
    public User getUserById( @PathVariable("id") Long id){
        return userService.findUserById(id);
    }

    @GetMapping
    public String check(){
        return "{\"status\":\"200\"}";
    }
}
