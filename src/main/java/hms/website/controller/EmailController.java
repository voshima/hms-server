package hms.website.controller;

import hms.website.services.EmailService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;

@RestController
@RequestMapping("/email")
public class EmailController {
    private final EmailService emailService;
    private final String to;
    private final String pathToFile;

    public EmailController(EmailService emailService, @Value("${spring.mail.to}") String to, @Value("${spring.servlet.multipart.location}") String pathToFile) {
        this.emailService = emailService;
        this.to = to;
        this.pathToFile = pathToFile;
    }

    @SneakyThrows
    @PostMapping
    public void sendEmail(@RequestParam("position") String position, @RequestParam("email") String email,
                          @RequestParam("fname") String first_name, @RequestParam("lname") String last_name,
                          @RequestParam(value = "file", required = false) MultipartFile file,
                          @RequestParam(value = "cover_letter", required = false, defaultValue = "none") String cover_letter) {
        String subject = String.format("Apply to %s", position);
        String message = String.format("%s %s want to apply to position %s\nCover letter:\n%s\nTo contact: %s", first_name, last_name, position, cover_letter, email);
        if(!file.isEmpty()) {
            File convFile = new File(pathToFile + "/" + file.getOriginalFilename());
            convFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(file.getBytes());
            fos.close();
            emailService.sendMessage(to, subject, message, convFile.getAbsolutePath());
        }
        else
        {
            emailService.sendMessage(to, subject, message);
        }
    }
}
