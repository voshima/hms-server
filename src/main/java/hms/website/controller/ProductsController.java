package hms.website.controller;

import hms.website.domain.Product;
import hms.website.services.ProductService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductsController {
    private final ProductService productService;

    public ProductsController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public List<Product> getProducts() {
        return productService.getAll();
    }

    @GetMapping("{id}")
    public Product getProduct(@PathVariable("id") Long id) { return productService.getById(id); }

}
