package hms.website.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import hms.website.utils.StringListConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
//@ToString(of={"id", "text", "hasImg", "src", "underImg"})
@Data
public class ChapterBlock {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "chapterId")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonBackReference
    private Chapter chapter;

    @Column(columnDefinition="text")
    @Convert(converter = StringListConverter.class)
    private List<String> text;
    private boolean hasImg;
    private String src;
    private String underImg;
}
