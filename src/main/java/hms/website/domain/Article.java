package hms.website.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.*;

@Data
@Entity
@Table
//@ToString(of = {"id", "head", "date", "author"})
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "articleId")
    private Long id;

    private String previewImg;

    private String head;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Date date = new Date();

    private String author;

    @OneToMany(mappedBy = "article")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonManagedReference
    @OrderBy("chapter_id")
    private List<Chapter> chapters = new ArrayList<>();
}
