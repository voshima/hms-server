package hms.website.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
@Entity
@Data
//@ToString(of = {"id", "name"})
public class Chapter {

    @Id
    @Column(name = "chapter_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "articleId")
    @ToString.Exclude
    @JsonBackReference
    private Article article;

    private String name;

    @OneToMany(mappedBy = "chapter")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonManagedReference
    @OrderBy("id")
    private List<ChapterBlock> blocks = new ArrayList<>();
}
